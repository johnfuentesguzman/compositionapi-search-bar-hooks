import  { ref, computed, watch } from 'vue';

export default function useSearch(items, filteredBy) {
    const enteredSearchTerm = ref('');
    const activeSearchTerm = ref('');

    const availableItems = computed(function () {
      let filterItems = [];
      if (activeSearchTerm.value) {
        filterItems = items.value.filter((usr) =>
          usr[filteredBy].includes(activeSearchTerm.value) // filtering by func parameter
        );
      } else if (items.value) {
        filterItems = items.value;
      }
      return filterItems;
    });

    watch(enteredSearchTerm, function (newValue) {
      setTimeout(() => {
        if (newValue === enteredSearchTerm.value) {
          activeSearchTerm.value = newValue;
        }
      }, 300);
    });

    function updateSearch(val) {
      enteredSearchTerm.value = val;
    }

    return {
        enteredSearchTerm,
        availableItems,
        updateSearch
    }; // your able to return array or an object, in order to expose functions to components

};